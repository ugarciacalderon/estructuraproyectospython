from setuptools import setup
import os

setup(
    name='Calculadora',
    version=1,
    description='Paquete basico para una calculadora',
    author='Ulises García',
    author_email='ugarciacalderon@gmail.com',
    license='GPL',
    packages=['calculadora'],
    url='git@gitlab.com:ugarciacalderon/estructuraproyectospython.git',
    zip_safe=False
)

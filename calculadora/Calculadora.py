class Calculadora():
    """
    Ejemplo de CI para gitlab de una calculadora que realiza operaciones basicas
    - Tabien se muestra la estructura de un proyecto python
    """
    def __init__(self):
       pass

    def suma(self, arg1, arg2):
        return arg1 + arg2

    def resta(self, arg1, arg2):
        return arg1 - arg2

    def multiplicacion(self, arg1, arg2):
        return arg1 * arg2

    def division(self, arg1, arg2):
        return arg2 / arg1 if arg2 > arg1 else arg1 / arg2

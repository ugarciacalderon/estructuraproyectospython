from Calculadora.Calculadora import *

cal = Calculadora()

class TestCalculadora:

    def test_suma(self):
        assert 13 == cal.suma(7,6)

    def test_resta(self):
        assert 5 == cal.resta(7,6)
